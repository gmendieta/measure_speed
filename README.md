# Measure Speed

*Measure Speed* is a Blender addon that draws the instant sped of one or several animated objects, specified by the user, in the 3d Viewport. It's a non destructive addon, that is to say, it doesn't modify any object in the Scene. Moreover, it provides a useful Panel with multiple options that allows the user to modify the way the measures are drawn.

# Installation

## How to install?

1. Uninstall any old version of Measure Speed, just in case ;)
2. Download the addon source code from the [addon page](https://gitlab.com/minininja/blender/addons/measure_speed), os simply click [here](https://gitlab.com/minininja/blender/addons/measure_speed/-/archive/master/measure_speed-master.zip)
3. Open Blender (version 2.8)
4. Go to *User Preferences*, *Addons* tab.
5. Click *Install from File* and choose the *zip file* you have just downloaded.
6. Activate the addon.
7. If you want the addon to be activated the next time you open Blender, save the user preferences at this point.

## How to uninstall?

1. Go to *User Preferences*, *Addons* tab.
2. Deactivate the addon.
3. If you want to delete the addon from the filesystem, unfold the addon preferences and Click *Remove*.

## Usage

After activate *Measure Speed* addon, the next Panel will be shown in the UI panel:

![alt](images/panel.png)




