# -*- coding: utf-8 -*-

bl_info = {
    "name": "Measure Speed",
    "description": "Measure Speed of objects and draw it in the 3D view",
    "author": "Gorka Mendieta",
    "version": (0, 2, 0),
    "blender": (2, 80, 0),
    "location": "View3D",
    "wiki_url": "https://gitlab.com/minininja/blender/addons/measure_speed/blob/master/README.md",
    "category": "3D View"
    }


# Import
import bpy
import importlib

from bpy.types import (
    Scene,
)

from bpy.props import (
    PointerProperty,
)

from . import measure_speed_op
from . import measure_speed_pt
from . import measure_speed_prop

classes = (
    measure_speed_op.MEASURE_SPEED_OT_toogle_measure_speed,
    measure_speed_op.MEASURE_SPEED_OT_add_measure,
    measure_speed_op.MEASURE_SPEED_OT_remove_measure,
    measure_speed_op.MEASURE_SPEED_OT_remove_active_measure,
    measure_speed_op.MEASURE_SPEED_OT_remove_all_measures,
    measure_speed_op.MEASURE_SPEED_OT_select_active_measure,
    measure_speed_pt.MEASURE_SPEED_UL_measures,
    measure_speed_pt.MEASURE_SPEED_PT_measure_speed,
    measure_speed_pt.MEASURE_SPEED_PT_measure_list,
)

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)

    Scene.measure_speed = PointerProperty(type=measure_speed_prop.MEASURE_SPEED_settings)


def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)

    del Scene.measure_speed

    # Unregister handle
    measure_speed_op.MEASURE_SPEED_OT_toogle_measure_speed.unregister_draw_handle(bpy.context)


if __name__ == "__main__":
    register()
