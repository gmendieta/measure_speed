# -*- coding: utf-8 -*-
"""
"""
import gpu
import bgl
from mathutils import Vector
from bpy_extras import view3d_utils
from gpu_extras.batch import batch_for_shader

# Dictionary to cache Batches. Extra attention to already used names
_batch_cache = {}

def get_coord_2d(context, coord_3d):
    """ 
    Converts a Vector 3d into a Vector 2d with projected screen coordinates
    """

    region = None   # Instance of Region
    rv3d = None     # Instance of RegionView3D 
    if context.area.type == 'VIEW_3D':
        region = context.region
        # When not Quad Split region_quadviews is empty
        if not context.space_data.region_quadviews:
            rv3d = context.space_data.region_3d
        else:
            quad_idx = 0
            # Get the current QuadView
            for quad_region in context.area.regions:
                if quad_region.type == 'WINDOW' and quad_region.alignment == 'QUAD_SPLIT':
                    if region == quad_region:
                        rv3d = context.space_data.region_quadviews[quad_idx]
                        break
                    quad_idx += 1

    coord_2d = Vector()
    if region and rv3d:
        coord_2d = view3d_utils.location_3d_to_region_2d(region, rv3d, coord_3d)
    return coord_2d

def draw_quad_2d(pos, size, color):
    """
    Draw a Rectangle
    """
    shader = gpu.shader.from_builtin('2D_UNIFORM_COLOR')
    batch = _batch_cache.get('quad_2d')
    if batch is None:
        vertices = (
            (-0.5, -0.5), (0.5, -0.5),
            (0.5, 0.5), (-0.5, 0.5)
        )
        indices = (
            (0, 1, 2), (0, 2, 3)
        )
        batch = batch_for_shader(shader, 'TRIS', {"pos": vertices}, indices=indices)
        _batch_cache['quad_2d'] = batch

    bgl.glEnable(bgl.GL_BLEND)

    with gpu.matrix.push_pop():
        gpu.matrix.translate((pos[0], pos[1]))
        gpu.matrix.scale((size[0], size[1]))
        
        shader.bind()
        shader.uniform_float("color", (color[0], color[1], color[2], color[3]))
        batch.draw(shader)

    # Restore GL defaults
    bgl.glDisable(bgl.GL_BLEND)

def draw_line_2d(start, end, width, color):
    """
    Draw a 2d line
    """
    if width <= 0: return

    vertices = (
        (start[0], start[1]),
        (end[0], end[1])
    )

    shader = gpu.shader.from_builtin('2D_UNIFORM_COLOR')
    batch = batch_for_shader(shader, 'LINES', {"pos": vertices})

    bgl.glLineWidth(width)
    bgl.glEnable(bgl.GL_BLEND)

    shader.bind()
    shader.uniform_float("color", (color[0], color[1], color[2], color[3]))
    batch.draw(shader)

    # Restore GL defaults
    bgl.glLineWidth(1)
    bgl.glDisable(bgl.GL_BLEND)
