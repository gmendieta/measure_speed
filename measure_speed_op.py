# -*- coding: utf-8 -*-
"""
"""

import bpy
import blf
from mathutils import Vector
from .gpu_utils import (
    get_coord_2d,
    draw_quad_2d,
    draw_line_2d,
)
from .units_utils import (
    get_length_acronym,
    get_meter_to_unit_scale,
    get_time_acronym,
    get_sec_to_unit_scale,
)

_font_id = 0
_font_dpi = 72
_font_padding = 10

def clean_measures(context):
    """"""
    settings = context.scene.measure_speed
    # Clean measures
    ob_name_list = [measure.ob_name for measure in settings.measure_list if measure.ob_name not in bpy.data.objects]
    for ob_name in ob_name_list:
        idx = settings.measure_list.find(ob_name)
        settings.measure_list.remove(idx)

def update_measures(context):
    """ """
    # First clean the possibly deleted objects
    clean_measures(context)

    settings = context.scene.measure_speed
    # Not update anything if we are in the same frame
    frame_current = context.scene.frame_current
    if settings.frame == frame_current:
        return

    for measure in settings.measure_list:
    # Calculate current Speed
        world_pos = bpy.data.objects[measure.ob_name].matrix_world.translation.copy()
        pos_delta = world_pos - measure.ob_pos
        measure.ob_delta = pos_delta.length
        # Store current Location
        measure.ob_pos = world_pos

    settings.frame = frame_current

def draw_measures(context):
    """"""
    if context.area.type != 'VIEW_3D':
        return
    update_measures(context)
    scene = context.scene
    fps = scene.render.fps
    settings = scene.measure_speed
    label_offset = settings.label_offset
    bpy_data_objects = bpy.data.objects # Reference to bpy.data.objects

    length_unit = scene.unit_settings.length_unit
    time_unit = scene.unit_settings.time_unit
    length_str = get_length_acronym(length_unit)
    time_str = get_time_acronym(time_unit)
    # Calculate the Units string and the Scale factor
    units = length_str + '/' + time_str
    length_scale = get_meter_to_unit_scale(length_unit) * (1.0 / get_sec_to_unit_scale(time_unit))
    
    blf.size(_font_id, settings.font_size, _font_dpi)
    blf.color(_font_id, settings.font_color[0], settings.font_color[1], settings.font_color[2], settings.font_color[3])
    for measure in settings.measure_list:
        # World location + Offset
        ob_coord_3d = bpy_data_objects[measure.ob_name].matrix_world.translation
        label_coord_3d = ob_coord_3d + label_offset
        ob_coord_2d = get_coord_2d(context, ob_coord_3d)
        label_coord_2d = get_coord_2d(context, label_coord_3d)

        # Generate Label string
        speed = measure.ob_delta * fps * length_scale # Depends on Units
        label_str = f"{speed:.{settings.speed_prec}f} {units}"
        label_size = blf.dimensions(_font_id, label_str)

        # Draw Line
        draw_line_2d(ob_coord_2d, label_coord_2d, settings.line_width, settings.bg_color)
        # Draw Quad. Its drawn from center and scaled, and Text is drawn from left so calculate
        quad_coord_2d = (label_coord_2d[0] + label_size[0] / 2.0, label_coord_2d[1] + label_size[1] / 2.0)
        label_size = (label_size[0] + _font_padding, label_size[1] + _font_padding)

        draw_quad_2d(quad_coord_2d, label_size, settings.bg_color)
        blf.position(_font_id, label_coord_2d[0], label_coord_2d[1], 0.0)
        # Using f-Literal string interpolation
        blf.draw(_font_id, label_str)


class MEASURE_SPEED_OT_toogle_measure_speed(bpy.types.Operator):
    bl_idname = "measure_speed.toogle_enable"
    bl_label = "Toogle Measure Speed"
    bl_description = "Toogle Measure Speed"
    bl_options = {'REGISTER'}

    _post_pixel_handle = None

    @staticmethod
    def register_draw_handle(context):
        if MEASURE_SPEED_OT_toogle_measure_speed._post_pixel_handle is None:
            MEASURE_SPEED_OT_toogle_measure_speed._post_pixel_handle = bpy.types.SpaceView3D.draw_handler_add(
                draw_measures, (context,), 'WINDOW', 'POST_PIXEL')


    @staticmethod
    def unregister_draw_handle(context):
        if MEASURE_SPEED_OT_toogle_measure_speed._post_pixel_handle is not None:
            bpy.types.SpaceView3D.draw_handler_remove(MEASURE_SPEED_OT_toogle_measure_speed._post_pixel_handle, 'WINDOW')
            MEASURE_SPEED_OT_toogle_measure_speed._post_pixel_handle = None

    @classmethod
    def poll(self, context):
        return True
    
    def execute(self, context):
        context.scene.measure_speed.enabled = not context.scene.measure_speed.enabled
        if context.scene.measure_speed.enabled:
            self.register_draw_handle(context)
        else:
            self.unregister_draw_handle(context)

        return {'FINISHED'}

class MEASURE_SPEED_OT_add_measure(bpy.types.Operator):
    bl_idname = "measure_speed.add_measure"
    bl_label = "Add measure"
    bl_description = "Add measures from selected objects"
    bl_options = {'REGISTER'}

    @classmethod
    def poll(self, context):
        objects = context.selected_objects
        if objects:
            return True
        else:
            return False

    def execute(self, context):
        if context.area.type != 'VIEW_3D':
            self.report({'WARNING'}, "View3D not found. Cannot run operator")
            return {'CANCELLED'}

        measure_list = context.scene.measure_speed.measure_list
        for ob in context.selected_objects:
            idx = measure_list.find(ob.name)
            if idx >= 0:
                continue
            measure = measure_list.add()
            measure.name = ob.name
            measure.ob_name = ob.name
            # Store current Location
            measure.ob_pos = bpy.data.objects[ob.name].matrix_world.translation.copy()

        return {'FINISHED'}

class MEASURE_SPEED_OT_remove_measure(bpy.types.Operator):
    bl_idname = "measure_speed.remove_measure"
    bl_label = "Remove measure"
    bl_description = "Remove measure from selected objects"
    bl_options = {'REGISTER'}

    @classmethod
    def poll(self, context):
        ob = context.selected_objects
        if ob:
            return True
        else:
            return False

    def execute(self, context):
        if context.area.type != 'VIEW_3D':
            self.report({'WARNING'}, "View3D not found. Cannot run operator")
            return {'CANCELLED'}

        measure_list = context.scene.measure_speed.measure_list
        for ob in context.selected_objects:
            idx = measure_list.find(ob.name)
            if idx >= 0:
                measure_list.remove(idx)

        return {'FINISHED'}

class MEASURE_SPEED_OT_remove_active_measure(bpy.types.Operator):
    bl_idname = "measure_speed.remove_active_measure"
    bl_label = "Remove active measure"
    bl_description = "Remove active measure in panel"
    bl_options = {'REGISTER'}

    @classmethod
    def poll(self, context):
        measure_speed = context.scene.measure_speed
        if len(measure_speed.measure_list) > 0 and measure_speed.active_measure_index >= 0:
            return True
        else:
            return False

    def execute(self, context):
        measure_speed = context.scene.measure_speed
        measure_speed.measure_list.remove(measure_speed.active_measure_index)

        return {'FINISHED'}

class MEASURE_SPEED_OT_remove_all_measures(bpy.types.Operator):
    bl_idname = "measure_speed.remove_all_measures"
    bl_label = "Remove all the measures"
    bl_description = "Remove all the measures"
    bl_options = {'REGISTER'}

    @classmethod
    def poll(self, context):
        return True

    def execute(self, context):
        scene = context.scene
        scene.measure_speed.measure_list.clear()
        return {'FINISHED'}


class MEASURE_SPEED_OT_select_active_measure(bpy.types.Operator):
    bl_idname = "measure_speed.select_active_measure"
    bl_label = "Select active measure object"
    bl_description = "Select active measure object"
    bl_options = {'REGISTER'}

    @classmethod
    def poll(self, context):
        measure_speed = context.scene.measure_speed
        if len(measure_speed.measure_list) > 0 and measure_speed.active_measure_index >= 0:
            return True
        else:
            return False

    def execute(self, context):
        if context.area.type != 'VIEW_3D':
            self.report({'WARNING'}, "View3D not found. Cannot run operator")
            return {'CANCELLED'}
        
        measure_speed = context.scene.measure_speed
        measure = measure_speed.measure_list[measure_speed.active_measure_index]
        # Deselect all objects and select the active
        bpy.ops.object.select_all(action='DESELECT')
        bpy.data.objects[measure.ob_name].select_set(True)

        return {'FINISHED'}

        

