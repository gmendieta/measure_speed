# -*- coding: utf-8 -*-
"""

"""

import bpy
from bpy.types import Panel, UIList

_measure_rows = 3

class MEASURE_SPEED_UL_measures(UIList):

    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        # assert(isinstance(item, bpy.types.MaterialSlot)
        # ob = data
        slot = item
        measure = item
        ob_name = measure.ob_name
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            layout.label(text=ob_name, icon='OBJECT_DATA')
        elif self.layout_type == 'GRID':
            layout.alignment = 'CENTER'
            layout.label(text="", icon='OBJECT_DATA')


class MEASURE_SPEED_PT_measure_list(Panel):
    bl_label = "Measure List"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_parent_id = "MEASURE_SPEED_PT_measure_speed"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False  # No animation.
        measure_speed = context.scene.measure_speed
        
         # Draw Object list
        row = layout.row()
        row.template_list("MEASURE_SPEED_UL_measures", "", measure_speed, "measure_list", measure_speed, "active_measure_index", rows=_measure_rows)

        col = row.column(align=True)
        col.operator("measure_speed.select_active_measure", icon='RESTRICT_SELECT_OFF', text="")
        col.operator("measure_speed.remove_active_measure", icon='REMOVE', text="")


class MEASURE_SPEED_PT_measure_speed(Panel):
    bl_label = "Measure Speed"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category= 'View'

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False  # No animation.
        measure_speed = context.scene.measure_speed

        if measure_speed.enabled is True:
            icon = "PAUSE"
            text = "Pause"
        else:
            icon = "PLAY"
            text = "Play"

        row = layout.row(align=True)
        row.operator("measure_speed.toogle_enable", text=text, icon=icon)
        row.operator("measure_speed.add_measure", text="", icon='ADD')
        row.operator("measure_speed.remove_measure", text="", icon='REMOVE')
        row.operator("measure_speed.remove_all_measures", text="", icon='PANEL_CLOSE')
        row = layout.row()
        row.prop(measure_speed, "speed_prec", text="Precission")
        row = layout.row()
        row.prop(measure_speed, "font_size", text="Font size")
        row = layout.row()
        row.prop(measure_speed, "font_color", text="Font color")
        row = layout.row()
        row.prop(measure_speed, "bg_color", text="Background color")
        row = layout.row()
        row.prop(measure_speed, "line_width", text="Line width")
        col = layout.column()
        col.prop(measure_speed, "label_offset", text="Label offset")

       # Measure List is a child panel
        

