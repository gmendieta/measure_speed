# -*- coding: utf-8 -*-
"""
"""

# Dictionary from Blender units to Real acronym
_time_unit_to_acronym = {
    'MICROSECONDS':     'μs',
    'MILLISECONDS':     'ms',
    'SECONDS':          's',
    'MINUTES':          'min',
    'HOURS':            'h',
    'DAYS':             'd',
    'ADAPTIVE':         'u', # Blender Units
}

_length_unit_to_acronym = {    
    # Metric System
    'MICROMETERS':      'μs',
    'MILLIMETERS':      'mm',
    'CENTIMETERS':      'cm',
    'METERS':           'm',
    'KILOMETERS':       'km',
    # Imperial System
    'THOU':             'th',
    'INCHES':           'in',
    'FEET':             'ft',
    'MILES':            'mi',
    'ADAPTIVE':         'u', # Blender Units
}

_sec_to_unit = {
    'MICROSECONDS':     1000000.0,
    'MILLISECONDS':     1000.0,
    'SECONDS':          1.0,
    'MINUTES':          0.0166667,
    'HOURS':            0.00027777833333,
    'DAYS':             1.157409722208333465e-5,
    'ADAPTIVE':         1.0, # Blender Units
}

_meter_to_unit = {
     # Metric System
    'MICROMETERS':      1000000.0,
    'MILLIMETERS':      1000.0,
    'CENTIMETERS':      100.0,
    'METERS':           1.0,
    'KILOMETERS':       0.001,
    # Imperial System
    'THOU':             39370.1,
    'INCHES':           39.3701,
    'FEET':             3.280841666667,
    'MILES':            0.000621371,
    'ADAPTIVE':         1.0, # Blender Units
}

def get_time_acronym(unit):
    """
    Get the standard time acronym for a unit
    """
    return _time_unit_to_acronym.get(unit, '??')

def get_length_acronym(unit):
    """
    Get the standard length acronym for a unit
    """
    return _length_unit_to_acronym.get(unit, '??')

def get_sec_to_unit_scale(unit):
    """
    Convert a second to another time unit
    """
    return _sec_to_unit.get(unit, 1.0)

def get_meter_to_unit_scale(unit):
    """
    Convert a blender unit to another length unit
    """
    return _meter_to_unit.get(unit, 1.0)








